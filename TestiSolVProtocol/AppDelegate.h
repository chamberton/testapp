//
//  AppDelegate.h
//  TestiSolVProtocol
//
//  Created by Serge on 2015/04/02.
//  Copyright (c) 2015 Serge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

