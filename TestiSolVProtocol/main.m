#import  <iSolvLibProtocol/iSolv_API_Wrapper.h>
#import <iSolvLibProtocol/Client_InviteSender.h>
#import <iSolvLibProtocol/Client_processSMSEnc.h>
#import "ViewController.h"

iSolv_API_Wrapper    *Wrapper;
Client_processSMSEnc *smsSender;
Client_InviteSender  *inviteSender;
Client_processMessageSimple* messageSimpleClient;
Client_processMessage* cpMsg;
struct networkPacket dataToSendForSms;
struct networkPacket dataToSendForInvite;
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UITextField *textOutput;
@property (strong,readonly)  iSolv_API_Wrapper* Wrapper;
@end



NSString* myNumber =@"1234567" ;
NSString* hisNumber=@"7654321";
NSString* message  =@"hello myself";
NSString* _time     =@"2014-Sep-26 18:07:54 +0200";

NSString* receiverPublic=@"-----BEGIN PUBLIC KEY-----\n"
"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDIiOP4y+pBFxmH4uVmxXk8ql3s\n"
"zqRRON589U1mJhM4GNbKyZEaSQqeUaDrBlbmg/vsgctsMn6s6SbCyVfcUsOfn3GM\n"
"WLo3Xf5gfw/qI5m+EaBt0Lhq1klv3wbRisRF1JGfiU2VipjIddi2YZN1zVPK+G+R\n"
"Jd1ZKhU4HEj+HOWQ4QIDAQAB\n"
"-----END PUBLIC KEY-----";

NSString* skey =@"-----BEGIN PRIVATE KEY-----\n"
"MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMiI4/jL6kEXGYfi\n"
"5WbFeTyqXezOpFE43nz1TWYmEzgY1srJkRpJCp5RoOsGVuaD++yBy2wyfqzpJsLJ\n"
"V9xSw5+fcYxYujdd/mB/D+ojmb4RoG3QuGrWSW/fBtGKxEXUkZ+JTZWKmMh12LZh\n"
"k3XNU8r4b5El3VkqFTgcSP4c5ZDhAgMBAAECgYBH+KFhLMZRQBogo/VTMjJ17Gsz\n"
"T2K35+IqFNwHekjRjFCijT8voOFXutI3J5pOtzXWRyscFO9T3YXUyPBomaetYgah\n"
"91IE0dkA6Hdi/7xREOeeh271MUgeXlgR+ivt6CzbF5mpS/3gQJRlVA5nGx9xKLQT\n"
"GGJJNWULI73v1IawFQJBAPdqwh7Z0JoRHQtynDcFN7nATx+77VTZUmuTchpHG1ce\n"
"hnzJMWY392x6MW+PC8GW6q4kZkyYADqDhzdxflqzJ4sCQQDPfcia2inWuaecZ3Ii\n"
"Lg4bRDmmcgTB0tuFr/L1AFZ4gADTyDiAYLoRDpxc0uQJpGJiPxw3PP42D7Lo5pEo\n"
"KZbDAkANj4RkPHv6dcWpwvZc7aRIQgoUKzCu3dpumv4+Ane1Jq8oV76lY5vkDi53\n"
"dtemHKdwM/6HZC1fB6Z5m3bZcchRAkEAy9TYRe89nLhDRO2GfLdbmkOakr19mN7I\n"
"Unt3pGFRkQzGQ0Z04wCiqPD+gF2YMTSwha1TzZv4ayle61bdkGPSuwJBAO0fgup8\n"
"yZ67R1E/ud/+ghPLn2oxy1U9FRAsiDG/BDAp8IPsrKH7H67tmN7fU1fkQswWoNgU\n"
"84VWIDQcyaP2es8=\n"
"-----END PRIVATE KEY-----\n";

@implementation ViewController

- (instancetype)init{
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    inviteSender=[[Client_InviteSender alloc] initWithSender:myNumber andPublicKey:nil andSecretKet:nil];
    
    smsSender=[[Client_processSMSEnc alloc] initWithSender:myNumber andPublicKey:receiverPublic andSecretKet:skey];
    
    messageSimpleClient=[[Client_processMessageSimple alloc] init];
    
    cpMsg=[[Client_processMessage alloc] init];
    
    Wrapper=[[iSolv_API_Wrapper alloc] initWithClient:cpMsg smsClient:smsSender messagesimpleclient:messageSimpleClient inviteClient:inviteSender];
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)callMethods :(id)sender{
    
    
    @autoreleasepool {
        
        [self buttonPress];
    }
}

- (void) buttonPress{
    
    
    
    
    int l=0;
    void* startDataTOsend=[Wrapper Ping:&l];
    
    
    
    startDataTOsend=[Wrapper Ack:&l];
    
    // you wont get the data to send, it would be ready after some data has been received
    [inviteSender sendInviteTo:hisNumber withMessage:message atTime:_time ];
    
    
    
    // In the case of a sending a sms you get the data to send
    startDataTOsend=[smsSender smsSend:hisNumber withContent:message hisPublic:receiverPublic timeStamp:_time lengthOfDataReturned:&l command:1];
    
    // you now need to keep monitoring if there is any data to be sent
    // while for ever
    dataToSendForSms.packet=[inviteSender getNextPacket:&l];
    if (l>0)
    {
        // you must then send the void* dataToSendForSms.packet , its length is l
    }
    dataToSendForInvite.packet=[smsSender getNextPacket:&l];
    if (l>0)
    {
        // you must then send the void*  dataToSendForInvite.packet , its length is l
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

@end