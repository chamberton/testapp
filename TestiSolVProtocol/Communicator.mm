//
//  Communicator.m
//  MKNetworkKit-iOS-Demo
//
//  Created by Isolv on 2015/05/04.
//  Copyright (c) 2015 Steinlogic. All rights reserved.
//
#include <string>
#import "Communicator.h"




NSInputStream * inputStream;
NSOutputStream * outputStream;

NSString* readString;

@implementation Communicator

- (void)setup{
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    NSURL *url = [NSURL URLWithString:host];
    
    NSLog(@"Setting up connection to %@ : %i", [url absoluteString], port);
    
    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"10.10.10.170", 9877, &readStream, &writeStream);
    
    inputStream = (NSInputStream *)CFBridgingRelease(readStream);
    outputStream = (NSOutputStream *)CFBridgingRelease(writeStream);
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    if(!CFWriteStreamOpen(writeStream)) {
        NSLog(@"Error, writeStream not open");
        
        return;
    }
    [self open];
    
    NSLog(@"Status of outputStream: %i", [outputStream streamStatus]);
    
    return;
}

- (void)open {
    NSLog(@"Opening streams.");
    
    
    [inputStream open];
    [outputStream open];
}

- (void)close {
    NSLog(@"Closing streams.");
    
    [inputStream close];
    [outputStream close];
    
    [inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    
    [inputStream setDelegate:nil];
    [outputStream setDelegate:nil];
    
    
    
    inputStream = nil;
    outputStream = nil;
}
- (NSStream*) getStream{
    return  outputStream;
}
- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)event {
    NSLog(@"Stream triggered.");
    
    switch(event) {
        case NSStreamEventHasSpaceAvailable: {
            if(stream == outputStream) {
                NSLog(@"outputStream is ready.");
            }
            break;
        }
        case NSStreamEventHasBytesAvailable: {
            if(stream == inputStream) {
                NSLog(@"inputStream is ready.");
                std::string str="";
                 unsigned int len = 0;
                NSMutableData* data=[[NSMutableData alloc] initWithLength:0];
                
              while ([inputStream hasBytesAvailable]) {
                  uint8_t* buf= new uint8_t[1024];
                  
                    len = [inputStream read:buf maxLength:sizeof(buf)/sizeof(*buf)];
                  
                    if (len > 0 || len==32) {
 
                        NSString *output = [[NSString alloc] initWithBytes:buf length:len encoding:NSASCIIStringEncoding];
                        //[data appendBytes: (const void *)buf length:len];
                        str=str+std::string([output UTF8String]);
                       
                    }
                 // delete [] buf;
                  
                }
                readString=[NSString stringWithUTF8String:str.c_str()];
                if (nil != readString) {
                    NSLog(@"server said: %@", readString);
                }
                /**
                len = [inputStream read:buf maxLength:1024];
                NSLog(@"Length read:%d",len);
                if(len > 0) {
                    NSMutableData* data=[[NSMutableData alloc] initWithLength:0];
                    
                    [data appendBytes: (const void *)buf length:len];
                    
                    NSString *s = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    
                    [self readIn:s];
                    
                   **/
                //}
                
                NSString *s = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                [self readIn:s];
            }
            break;
        }
        default: {
            NSLog(@"Stream is sending an Event: %i", event);
            
            break;
        }
    }
}
uint8_t  * convertToByteArray(const int& sequenceNumber) {
    const void* vPtr = &sequenceNumber;
    int size = 4; //sizeof(sequenceNumber);
    
    uint8_t *a = new uint8_t[size];
    for (int i = 0; i < size; i++) {
        a[size-i-1] = (int) *((uint8_t*) (vPtr)+ i);
        ///   cout << (int) a[i] << endl;
    }
    
    return a;
}

- (void)readIn:(NSString *)s {
    NSLog(@"Reading in the following:");
    NSLog(@"%@", s);
}

- (void)writeOut:(NSString *)s {
    uint8_t *buf = (uint8_t *)[s UTF8String];
    int length=strlen((char *)buf);
    NSLog(@"Length is %d",length);
    
    uint8_t* final=new uint8_t[4+length];
    
    uint8_t* array=convertToByteArray(length);
    
    for (int i=0; i<4+length; i++){
        final[i]=i < 4?array[i]:buf[i-4];
    }
    [outputStream write:final maxLength:length+4];
   
}
- (void) dealloc{
    [self close];
}
@end