//
//  ViewController.h
//  TestiSolVProtocol
//
//  Created by Serge on 2015/04/02.
//  Copyright (c) 2015 Serge. All rights reserved.
//
#import  <iSolvLibProtocol/iSolv_API_Wrapper.h>
#import <iSolvLibProtocol/Client_InviteSender.h>
#import <iSolvLibProtocol/Client_processSMSEnc.h>
#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@end

